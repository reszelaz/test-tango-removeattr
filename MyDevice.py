import time

import tango
from tango import Attr, AttrWriteType
from tango.server import LatestDeviceImpl, DeviceClass


class AttrTypeSwitcher:

    def __init__(self, types):
        self._types = types
        self._first_used = False    
    
    def get_type(self):
        if not self._first_used:
            self._first_used = True
            return self._types[0]
        else:
            return self._types[1]


class MyDevice(LatestDeviceImpl):

    attr_type_switcher = AttrTypeSwitcher((tango.DevDouble, tango.DevLong))

    def init_device(self):
        super().init_device()
        self.set_state(tango.DevState.ON)
    
    def initialize_dynamic_attributes(self):
        attr_info = {
            "attr1": tango.DevDouble,
            "attr2": None,  # first device -> DevDouble; next devices -> DevLong
            "attr3": tango.DevString
        }
        device_attr_names = []
        multi_attr = self.get_device_attr()
        for i in range(multi_attr.get_attr_nb()):
            device_attr_names.append(multi_attr.get_attr_by_ind(i).get_name())

        klass_attr_names = []
        dev_class = self.get_device_class()
        multi_class_attr = dev_class.get_class_attr()
        klass_attrs = multi_class_attr.get_attr_list()
        for ind in range(len(klass_attrs)):
            klass_attr_names.append(klass_attrs[ind].get_name())
        for attr_name, attr_type in attr_info.items():
            if attr_name in device_attr_names:
                print("remove device attribute: {}".format(attr_name))
                self.remove_attribute(attr_name)
            if attr_name == "attr2" and attr_name in klass_attr_names:
                attr = multi_class_attr.get_attr(attr_name)
                print("remove class attr: {}".format(attr_name))
                multi_class_attr.remove_attr(
                    attr.get_name(),
                    attr.get_cl_name()
                )
            if attr_type is None:
                attr_type = self.attr_type_switcher.get_type() 
            attr = Attr(
                attr_name, attr_type, AttrWriteType.READ_WRITE
            )
            self.add_attribute(attr, self.read_general, self.write_general)
    
    def read_general(self, attr):
        attr_name = attr.get_name()
        self.info_stream("Reading {}".format(attr_name))
        if attr_name == "attr1":
            attr.set_value(11.11)
        elif attr_name == "attr2":
            attr.set_value(22.22)
        elif attr_name == "attr3":
            attr.set_value("hello")
        # uncomment to simulate a longer read
        # time.sleep(0.1) 
    
    def write_general(self, attr):
        self.info_stream("Writting {}".format(attr.get_name()))


class MyDeviceClass(DeviceClass):

    def __init__(self, name):
        super().__init__(name)
        self.set_type(name)

    def dyn_attr(self, dev_list):
        for dev in dev_list:
            try:
                dev.initialize_dynamic_attributes()
            except Exception as e:
                print("Failed to initialize dynamic attributes")
                print(e)
