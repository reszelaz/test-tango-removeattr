import tango

try:
    pool = tango.DeviceProxy("test/pooldevice/1")
    i = 0
    while True:
        print("create device {}".format(i))
        pool.CreateDevice(["MyDevice", "test/mydevice/2"])
        print("delete device {}".format(i))
        pool.DeleteDevice(["MyDevice", "test/mydevice/2"])
        i += 1
except KeyboardInterrupt:
    try:
        print("trying to delete device {}".format(i))
        pool.DeleteDevice(["MyDevice", "test/mydevice/2"])
    except:
        pass
