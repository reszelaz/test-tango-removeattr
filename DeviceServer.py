from tango.server import run

from PoolDevice import PoolDevice
from MyDevice import MyDevice, MyDeviceClass

run([PoolDevice, (MyDeviceClass, MyDevice)])
