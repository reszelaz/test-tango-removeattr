# Try to reproduce problems with removing attributes

@HEnquist reported Pool segfault when removing attributes from the device or device class
at runtime. This happened when creating new devices in the Pool.

More information in: https://gitlab.com/sardana-org/sardana/-/merge_requests/1677

## Steps to reproduce

1. Register in Tango Database one `DeviceServer` DS with instance name `test`
   with:
   - 1 device of `PoolDevice` class, with the following name: `test/pooldevice/1`
   - 1 device of `MyDevice` class, with the following name: `test/mydevice/1`

    ```console
    tango_admin --add-server DeviceServer/test PoolDevice test/pooldevice/1
    tango_admin --add-server DeviceServer/test MyDevice test/mydevice/1    
    ```
2. Start `DeviceServer`: `python3 DeviceServer.py test`
3. Start reading client: `python3 client1.py`
4. Start device recreating client: `python3 client2.py`

## Result

After running the clients for tens of minutes the issue is not reproducible.
